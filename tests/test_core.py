import os
import sys
import pytest

import unittest

sys.path.insert(0, os.path.abspath("core"))
from core import open_file, count_url_status # noqa
sys.path.insert(0, os.path.abspath("tests"))
from default import LINE # noqa


class TestStringMethods(unittest.TestCase):

    def setUp(self):
        path = os.path.abspath("tests/")
        file = open(os.path.join(path, 'test.txt'), 'w')
        file.write('')
        file.close()

        file = open(os.path.join(path, 'test_with_values.txt'), 'w')
        file.write(LINE)
        file.close()

    def tearDown(self):
        os.remove(os.path.abspath("tests/test.txt"))
        os.remove(os.path.abspath("tests/test_with_values.txt"))

    def test_open_file(self):
        with pytest.raises(FileNotFoundError):
            file = open_file(os.path.abspath("logs/log1.txt")) # noqa

    def test_all(self):
        url, status = count_url_status(os.path.abspath("logs/log.txt"))
        self.assertTrue(url)
        self.assertTrue(status)

    def test_empty_file(self):
        url, status = count_url_status(os.path.abspath("tests/test.txt"))
        self.assertIsNone(url)
        self.assertIsNone(status)

    def test_count_url_status(self):
        url, status = count_url_status(os.path.abspath(
            "tests/test_with_values.txt")
        )
        self.assertEqual(len(url), 3)
        self.assertEqual(len(status), 3)
