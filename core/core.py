import os
import sys

from collections import defaultdict


def open_file(file_name):
    return open(file_name, 'r', encoding='ISO-8859-1')


def sorted_url_status(urls, status):
    urls = sorted(
        urls.items(),
        key=lambda x: x[1], reverse=True
    )
    status_code_count = sorted(
        status.items(),
        key=lambda x: x[1], reverse=True
    )
    return urls, status_code_count


def filter_data(parameter, data):
    return list(filter(lambda x: x.startswith(parameter), data))


def count_url_status(file_name=None):
    if not file_name:
        print('Filename required.')
        return
    if os.stat(file_name).st_size == 0:
        print('Empty file.')
        return None, None
    file = open_file(file_name=file_name)

    line = True
    urls_count = defaultdict(int)
    status_code_count = defaultdict(int)

    while line:
        line = file.readline()
        data = line.split(' ')

        url = filter_data('request_to', data)
        status_code = filter_data('response_status', data)

        if url:
            urls_count[url[0].split('=')[1]] += 1

        if status_code:
            status_code_count[status_code[0].split('=')[1]] += 1

    urls, status_code_count = sorted_url_status(
        urls=urls_count,
        status=status_code_count
    )

    print('URLS:')
    for item in range(0, 3):
        print('{} - {}'.format(*urls[item]))

    print('COUNT STATUS:')
    for status, count in status_code_count:
        print('{} - {}'.format(status.replace('\n', ''), count))

    return urls, status_code_count


if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_name = sys.argv[1]
        count_url_status(file_name=file_name)
