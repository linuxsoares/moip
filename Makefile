THIS_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
RUNTEST=py.test -v -x .
RUNTESTDEBUG=py.test --pdb -v -x .

ALLMODULES=$(patsubst %.py, %, $(wildcard test_*.py))

clean:
	@find . -name "*.pyc" | xargs rm -rf
	@find . -name "*.pyo" | xargs rm -rf
	@find . -name "__pycache__" -type d | xargs rm -rf
	@rm -f .coverage
	@rm -rf htmlcov/
	@rm -f coverage.xml
	@rm -f *.log

run:
	@python $(THIS_DIR)core/core.py $(THIS_DIR)logs/$(file_name)

test: clean
	${RUNTEST} ${ALLMODULES}

test-debug: clean
	${RUNTESTDEBUG} ${ALLMODULES}

% : test_%.py
	${RUNTEST} test_$@
