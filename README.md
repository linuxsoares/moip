# MOIP
Test MOIP

# Requisitos
- Python 3

# Desenvolvimento
Todos os comandos rodar testes, rodar a aplicação `Makefile` na raiz do projeto.

## Instalação
- Crie e ative uma [virtualenv](https://virtualenv.pypa.io/) com Python 3.

## Testes
- Para rodar os testes, execute:<br />
    `make test`
- Para rodar os testes com debug, execute:<br />
    `test-debug`

## Executando
```
O arquivo a ser analisado deve estar dentro do diretório `logs`.
```
- Rodando o projeto:
    `make run file_name='nome_do_arquivo'`
